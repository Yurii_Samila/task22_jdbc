package com.taskJDBC.model;

import com.taskJDBC.model.annotation.Column;
import com.taskJDBC.model.annotation.PrimaryKey;
import com.taskJDBC.model.annotation.Table;

@Table(name = "service_name")
public class Service_name {

  @PrimaryKey
  @Column(name = "id")
  private int id;
  @Column(name = "name")
  private String name;
  @Column(name = "intermediary_id")
  private int intermediary_id;

  public Service_name() {
  }

  public Service_name(int id, String name, int intermediary_id) {
    this.id = id;
    this.name = name;
    this.intermediary_id = intermediary_id;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getIntermediary_id() {
    return intermediary_id;
  }

  public void setIntermediary_id(int intermediary_id) {
    this.intermediary_id = intermediary_id;
  }

  @Override
  public String toString() {
    return "Service_name{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", intermediary_id=" + intermediary_id +
        '}';
  }
}

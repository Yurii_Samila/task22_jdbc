package com.taskJDBC.model;

import com.taskJDBC.model.annotation.Column;
import com.taskJDBC.model.annotation.PrimaryKey;
import com.taskJDBC.model.annotation.Table;

@Table(name = "intermediary")
public class Intermediary {

  @PrimaryKey
  @Column(name = "id")
  private int id;

  public Intermediary() {
  }

  public Intermediary(int id) {
    this.id = id;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return "Intermediary{" +
        "id=" + id +
        '}';
  }
}

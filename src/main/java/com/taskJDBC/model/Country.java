package com.taskJDBC.model;

import com.taskJDBC.model.annotation.Column;
import com.taskJDBC.model.annotation.PrimaryKey;
import com.taskJDBC.model.annotation.Table;

@Table(name = "country")
public class Country {

  @PrimaryKey
  @Column(name = "id")
  private int id;
  @Column(name = "name")
  private String name;

  public Country() {
  }

  public Country(int id, String name) {
    this.id = id;
    this.name = name;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "Country{" +
        "id=" + id +
        ", name='" + name + '\'' +
        '}';
  }
}

package com.taskJDBC.model;

import com.taskJDBC.model.annotation.Column;
import com.taskJDBC.model.annotation.PrimaryKey;
import com.taskJDBC.model.annotation.PrimaryKeyComposite;
import com.taskJDBC.model.annotation.Table;

@Table(name = "client")
public class Client {

  @PrimaryKeyComposite
  @Column(name = "id")
  private int id;
  @PrimaryKeyComposite
  @Column(name = "login")
  private String login;
  @Column(name = "document_id")
  private int document_id;
  @Column(name = "name")
  private String name;
  @Column(name = "surname")
  private String surname;
  @Column(name = "email")
  private String email;
  @Column(name = "phone_number")
  private String phone_number;
  @Column(name = "country_id")
  private int country_id;
  @Column(name = "city_id")
  private int city_id;
  @Column(name = "street_id")
  private int street_id;

  public Client() {
  }

  public Client(int id, String login, int document_id, String name, String surname,
      String email, String phone_number, int country_id, int city_id, int street_id) {
    this.id = id;
    this.login = login;
    this.document_id = document_id;
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.phone_number = phone_number;
    this.country_id = country_id;
    this.city_id = city_id;
    this.street_id = street_id;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public int getDocument_id() {
    return document_id;
  }

  public void setDocument_id(int document_id) {
    this.document_id = document_id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPhone_number() {
    return phone_number;
  }

  public void setPhone_number(String phone_number) {
    this.phone_number = phone_number;
  }

  public int getCountry_id() {
    return country_id;
  }

  public void setCountry_id(int country_id) {
    this.country_id = country_id;
  }

  public int getCity_id() {
    return city_id;
  }

  public void setCity_id(int city_id) {
    this.city_id = city_id;
  }

  public int getStreet_id() {
    return street_id;
  }

  public void setStreet_id(int street_id) {
    this.street_id = street_id;
  }

  @Override
  public String toString() {
    return "Client{" +
        "id=" + id +
        ", login='" + login + '\'' +
        ", document_id=" + document_id +
        ", name='" + name + '\'' +
        ", surname='" + surname + '\'' +
        ", email='" + email + '\'' +
        ", phone_number='" + phone_number + '\'' +
        ", country_id=" + country_id +
        ", city_id=" + city_id +
        ", street_id=" + street_id +
        '}';
  }
}

package com.taskJDBC.model;

import com.taskJDBC.model.annotation.Column;
import com.taskJDBC.model.annotation.PrimaryKey;
import com.taskJDBC.model.annotation.Table;
import java.util.Date;

@Table(name = "transaction")
public class Transaction {

  @PrimaryKey
  @Column(name = "id")
  private int id;
  @Column(name = "time")
  private Date time;
  @Column(name = "client_account_id")
  private int client_account_id;
  @Column(name = "service_account_id")
  private int service_account_id;
  @Column(name = "amount")
  private int amount;
  @Column(name = "intermediary_id")
  private int intermediary_id;

}

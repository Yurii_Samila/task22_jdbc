package com.taskJDBC.model;

import com.taskJDBC.model.annotation.Column;
import com.taskJDBC.model.annotation.PrimaryKey;
import com.taskJDBC.model.annotation.Table;

@Table(name = "service_account")
public class Service_account {

  @PrimaryKey
  @Column(name = "id")
  private int id;
  @Column(name = "number")
  private String number;
  @Column(name = "balance")
  private int balance;
  @Column(name = "intermediary_id")
  private int intermediary_id;

  public Service_account() {
  }

  public Service_account(int id, int balance, int intermediary_id) {
    this.id = id;
    this.number = (id + "_" + "S_ACC");
    this.balance = balance;
    this.intermediary_id = intermediary_id;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public int getBalance() {
    return balance;
  }

  public void setBalance(int balance) {
    this.balance = balance;
  }

  public int getIntermediary_id() {
    return intermediary_id;
  }

  public void setIntermediary_id(int intermediary_id) {
    this.intermediary_id = intermediary_id;
  }

  @Override
  public String toString() {
    return "Service_account{" +
        "id=" + id +
        ", number='" + number + '\'' +
        ", balance=" + balance +
        ", intermediary_id=" + intermediary_id +
        '}';
  }
}

package com.taskJDBC.model;

import com.taskJDBC.model.annotation.Column;
import com.taskJDBC.model.annotation.PrimaryKeyComposite;
import com.taskJDBC.model.annotation.Table;

@Table(name = "password")
public class Password {

  @Column(name = "password")
  private String password;
  @PrimaryKeyComposite
  @Column(name = "client_id")
  private int client_id;
  @PrimaryKeyComposite
  @Column(name = "client_login")
  private String client_login;

  public Password() {
  }

  public Password(String password, int client_id, String client_login) {
    this.password = password;
    this.client_id = client_id;
    this.client_login = client_login;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public int getClient_id() {
    return client_id;
  }

  public void setClient_id(int client_id) {
    this.client_id = client_id;
  }

  public String getClient_login() {
    return client_login;
  }

  public void setClient_login(String client_login) {
    this.client_login = client_login;
  }

  @Override
  public String toString() {
    return "Password{" +
        "password='" + password + '\'' +
        ", client_id=" + client_id +
        ", client_login='" + client_login + '\'' +
        '}';
  }
}

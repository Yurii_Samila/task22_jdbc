package com.taskJDBC.model;

import com.taskJDBC.model.annotation.Column;
import com.taskJDBC.model.annotation.PrimaryKey;
import com.taskJDBC.model.annotation.Table;

@Table(name = "street")
public class Street {

  @PrimaryKey
  @Column(name = "id")
  private int id;
  @Column(name = "name")
  private String name;
  @Column(name = "city_id")
  private int city_id;

  public Street() {
  }

  public Street(int id, String name, int city_id) {
    this.id = id;
    this.name = name;
    this.city_id = city_id;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getCity_id() {
    return city_id;
  }

  public void setCity_id(int city_id) {
    this.city_id = city_id;
  }

  @Override
  public String toString() {
    return "Street{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", city_id=" + city_id +
        '}';
  }
}

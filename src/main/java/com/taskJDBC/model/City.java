package com.taskJDBC.model;

import com.taskJDBC.model.annotation.Column;
import com.taskJDBC.model.annotation.PrimaryKey;
import com.taskJDBC.model.annotation.Table;

@Table(name = "city")
public class City {

  @PrimaryKey
  @Column(name = "id")
  private int id;
  @Column(name = "name")
  private String name;
  @Column(name = "country_id")
  private int country_id;

  public City() {
  }

  public City(int id, String name, int country_id) {
    this.id = id;
    this.name = name;
    this.country_id = country_id;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getCountry_id() {
    return country_id;
  }

  public void setCountry_id(int country_id) {
    this.country_id = country_id;
  }

  @Override
  public String toString() {
    return "City{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", country_id=" + country_id +
        '}';
  }
}

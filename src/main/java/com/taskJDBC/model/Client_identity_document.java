package com.taskJDBC.model;

    import com.taskJDBC.model.annotation.Column;
    import com.taskJDBC.model.annotation.PrimaryKey;
    import com.taskJDBC.model.annotation.Table;

@Table(name = "client_identity_document")
public class Client_identity_document {

  @PrimaryKey
  @Column(name = "id")
  private int id;
  @Column(name = "number")
  private String number;

}

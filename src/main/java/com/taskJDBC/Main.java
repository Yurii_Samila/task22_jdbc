package com.taskJDBC;

import com.taskJDBC.DAO.implementation.CountryDaoImpl;
import com.taskJDBC.model.Country;
import java.sql.SQLException;

public class Main {

  public static void main(String[] args) throws SQLException {
    CountryDaoImpl countryDao = new CountryDaoImpl();
    Country country = countryDao.findByName("Ukraine");
    System.out.println(country);

  }
}

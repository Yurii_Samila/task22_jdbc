package com.taskJDBC.DAO;

import com.taskJDBC.model.Country;
import java.sql.SQLException;
import java.util.List;

public interface CountryDAO extends MainDAO {
  Country findByName(String name) throws SQLException;
}

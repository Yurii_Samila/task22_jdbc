package com.taskJDBC.DAO;

import java.sql.SQLException;
import java.util.List;

public interface MainDAO<T, ID> {
  List<T> findAll() throws SQLException;
}

package com.taskJDBC.DAO.implementation;

import com.taskJDBC.DAO.CountryDAO;
import com.taskJDBC.connection.ConnectionDB;
import com.taskJDBC.model.Country;
import com.taskJDBC.transformer.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CountryDaoImpl implements CountryDAO {


  @Override
  public Country findByName(String name) throws SQLException {
    Country country = null;
    List<Country> countries = new ArrayList<>();
    Connection connection = ConnectionDB.getConnection();
    try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM country WHERE name = ?")) {
        statement.setString(1,name);
        try (ResultSet resultSet = statement.executeQuery()) {
      while (resultSet.next()) {
        country = (Country) new Transformer(Country.class).transformToEntity(resultSet);
      }
      }
    }
    return country;
  }

  @Override
  public List<Country> findAll() throws SQLException {
  List<Country> countries = new ArrayList<>();
    Connection connection = ConnectionDB.getConnection();
    try (Statement statement = connection.createStatement()){
      try (ResultSet resultSet = statement.executeQuery("SELECT * FROM country")) {
        while (resultSet.next()){
          countries.add((Country) new Transformer(Country.class).transformToEntity(resultSet));
        }
      }
    }
    return countries;
  }
}

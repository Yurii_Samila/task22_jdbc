package com.taskJDBC.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ConnectionDB {

  public static ResourceBundle resourceBundle = ResourceBundle.getBundle("properties");

  private static Connection connection = null;

  public ConnectionDB() {
  }

public static Connection getConnection(){
  if (connection == null){
    try {
      connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mybankdb?serverTimezone=UTC&useSSL=false&allowPublicKeyRetrieval=true",
                                               "root",
                                               "root");
}catch (SQLException e){
      System.out.println("SQLExeption: " + e.getMessage());
      System.out.println("SQLState: " + e.getSQLState());
      System.out.println("Error: " + e.getErrorCode());
    }
  }
  return connection;
  }
}
